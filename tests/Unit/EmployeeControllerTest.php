<?php

namespace Tests\Unit;

use App\Models\Company;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\User;
use App\Models\Employee;

class EmployeeControllerTest extends TestCase
{
    use DatabaseTransactions;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->employee = Employee::factory()->create();
        $this->company = Company::factory()->create();
        $this->user = User::where('email','admin@admin.com') -> first();
    }

    public function test_admin_can_see_employee_index_page()
    {
        $this->actingAs($this->user);
        
        $this->get('/employees')
            ->assertViewIs('employee.index');
    }

    public function test_admin_can_create_employee()
    {
        $this->actingAs($this->user);
        $employee = Employee::factory()->make();
        $this->post('/employees/', $employee->toArray())
            ->assertStatus(302);
        $this->get('/employees/' . $employee->id)
            ->assertOk();
    }
}
