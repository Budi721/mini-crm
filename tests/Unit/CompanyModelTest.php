<?php

namespace Tests\Unit;

use App\Models\Company;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class CompanyModelTest extends TestCase
{
    use DatabaseTransactions;
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_eloquent_company_hasmany_employee()
    {
        $company = Company::factory()->make();
        $this->assertIsObject($company->employee);
    }
}
