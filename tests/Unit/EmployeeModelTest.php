<?php

namespace Tests\Unit;

use App\Models\Employee;
// use PHPUnit\Framework\TestCase;
use Tests\TestCase;

use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmployeeModelTest extends TestCase
{
    use DatabaseTransactions;
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_eloquent_employee_belong_to_company()
    {
        $employee = Employee::factory()->make();
        $this->assertIsObject($employee->company);
    }
}
