<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\User;
use App\Models\Company;


class CompanyControllerTest extends TestCase
{
    use DatabaseTransactions;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->company = Company::factory()->create();
        $this->user = User::where('email','admin@admin.com') -> first();
    }

    public function test_admin_can_see_company_index_page()
    {
        $this->actingAs($this->user);
        $companies = $this->company;
        $this->get('/companies')
            ->assertSee($companies->name)
            ->assertOk();
    }

    public function test_admin_can_see_company_detail_page()
    {
        $this->actingAs($this->user);
        $companies = $this->company;
        $this->get('/companies/'.$companies->id)
            ->assertSee($companies->name)
            ->assertOk();
    }

    public function test_admin_can_create_company()
    {
        $this->actingAs($this->user);
        $company = Company::factory()->make();
        $this->post('/companies/', $company->toArray())
            ->assertStatus(302);
        $this->get('/companies/' . $company->id)
            ->assertOk();
    }

    public function test_admin_can_update_company()
    {
        $this->actingAs($this->user);
        $company = $this->company;
        $new_name = 'Comp Corp';
        $new_email = 'comp_corp@co.id';

        $this->put('/companies/'.$company->id, [
            'name' => $new_name,
            'email' => $new_email
        ])->assertStatus(302);

        // $this->get('/companies/'.$company->id)
        //     ->assertDontSee($company->name);
    }

    public function test_admin_can_delete_company()
    {
        $this->actingAs($this->user);
        $company = $this->company;

        $this->delete('/companies/'.$company->id)
            ->assertStatus(500);

        $this->get('/companies/'.$company->id)
            ->assertNotFound();
    }
}
