<?php

namespace Tests\Feature;

use App\Models\Company;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use App\Models\User;

class CompanyTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();
        $this->company = Company::factory()->make();
        $this->user = User::where('email','admin@admin.com') -> first();
    }

    public function test_user_cant_see_companypage()
    {
        $response = $this->get('/companies');

        $response
            ->assertRedirect('/login');
    }

    public function test_admin_can_see_companypage()
    {
        $this->actingAs($this->user);
        $response = $this->get('/companies');

        $response
            ->assertSee('DAFTAR COMPANY')
            ->assertStatus(200);
    }

}
