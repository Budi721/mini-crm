<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;
use App\Models\User;

class EmployeeTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::where('email','admin@admin.com') -> first();
    }

    public function test_admin_make_ajax_request_datatable()
    {
        $this->actingAs($this->user);
        $response = $this->get('/get-employees');

        $response->assertJson(fn (AssertableJson $json) =>
            $json->whereType('data', 'array')
                ->whereAllType([
                    'draw' => 'integer',
                    'recordsTotal' => 'integer',
                    'recordsFiltered' => 'integer',
                    'input' => 'array'
            ])
        );
    }
}
