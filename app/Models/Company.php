<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class Company extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $guarded = ['id'];
    
    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
}
