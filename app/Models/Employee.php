<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Employee extends Model
{
    use HasFactory;
    public $timestamps = false;
    public $guarded = ['id'];
    
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
