<?php

namespace App\Http\Controllers;

use App\Mail\CompanyCreated;
use App\Models\Company;
use Illuminate\Http\Request;
use Mail;

class CompanyController extends Controller
{
    public function index()
    {
        $companies = Company::orderBy('id', 'desc')->paginate(10);
        return view('company.index', compact('companies'));
    }

    public function show($id)
    {
        return view('company.show')
            ->with('company', Company::findOrFail($id));
    }

    public function create()
    {
        return view('company.create');
    }

    public function store(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'email' => 'email:rfc,dns',
            'logo' => 'image|mimes:jpg,jpeg,png|dimensions:min_width=100,min_height=100',
            'website' => ''
        ]);
        
        $imgName = null;
        if ($req->logo)
        {
            $imgName = $req->logo->getClientOriginalName() . 
                    '-' . time() . '.' . $req->logo->extension();
            $req->logo->move(public_path('images'), $imgName); 
        }

        $createdCompany = Company::create([
            'name' => $req->name,
            'email' => $req->email,
            'logo' => $imgName,
            'website' => $req->website
        ]);

        if ($createdCompany)
        {
            Mail::to($req->email)->send(new CompanyCreated(
                $createdCompany
            ));
        }

        return redirect('/companies');
    }

    public function edit($id)
    {
        return view('company.edit')
            ->with('company', Company::findOrFail($id));
    }

    public function update(Request $req, $id)
    {   
        $company = Company::find($id);
        $req->validate([
            'name' => 'required',
            'email' => 'email:rfc,dns',
            'logo' => 'image|mimes:jpg,jpeg,png|dimensions:min_width=100,min_height=100',
            'website' => ''
        ]);

        $updatedField = [
            'name' => $req->name,
            'email' => $req->email,
            'website' => $req->website
        ];
        
        if ($req->logo)
        {
            $imgName = $req->logo->getClientOriginalName() . 
                    '-' . time() . '.' . $req->logo->extension();
            $req->logo->move(public_path('images'), $imgName); 
            unlink("images/".$company->logo);
            $updatedField['logo'] = $imgName; 
        }

        Company::find($id)->update($updatedField);

        return redirect('/companies');
    }

    public function destroy($id)
    {
        $company = Company::find($id);
        if ($company->delete())
        { unlink("images/".$company->logo); }
        
        return redirect('/companies');
    }
  
}
