<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Company;
use DataTables;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    public function index()
    {
        return view('employee.index');
    }

    public function getEmployees()
    {
        $data = Employee::orderBy('id', 'desc')->get();
        return Datatables::of($data)
            ->addIndexColumn()
            ->addColumn('fullname', function ($row)
            {
                return $row->first_name.' '.$row->last_name;
            })
            ->addColumn('company', function ($row)
            {
                return $this->getCompanyName($row->company_id);
            })
            ->addColumn('action', function($row)
            {
                $btn = '<a href="/employees/'.$row->id.'/edit" class="edit btn btn-primary">Edit</a> 
                        <a href="'. route('employees.delete', ['id' => $row->id]) .'" class="delete btn btn-danger">Delete</a>';
                return $btn;
            })
            ->rawColumns(['fullname', 'company', 'action'])
            ->make(true);
    }

    public function getCompanyName($id)
    {
        $company = Company::find($id);
        if ($company)
            return $company->name;
        return '';
    }

    public function create()
    {
        $companies = Company::all();
        return view('employee.create')
            ->with('companies', $companies);
    }

    public function store(Request $req)
    {
        $req->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);
        
        Employee::create([
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'company_id' => $req->company_id,
            'email' => $req->email,
            'phone' => $req->phone,
        ]);

        return redirect('/employees');
    }

    public function edit($id)
    {   
        $companies = Company::all();
        return view('employee.edit', [
            'employee' => Employee::findOrFail($id),
            'companies' => $companies
        ]);
    }

    public function update(Request $req, $id)
    {
        $req->validate([
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        Employee::find($id)->update([
            'first_name' => $req->first_name,
            'last_name' => $req->last_name,
            'company_id' => $req->company_id,
            'email' => $req->email,
            'phone' => $req->phone,
        ]);

        return redirect('/employees');
    }

    public function destroy($id)
    {
        Employee::find($id)->delete();
        return redirect('/employees');
    }
}
