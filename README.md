## Before We Get Started

Tools yang harus dipersiapkan terlebih dahulu:

1. PHP version > 7.4
2. Composer version 2.0


## Setup

### Clone Repository

Lakukan clone repository, bisa menggunakan Sourcetree atau dengan CLI:

```ssh
https://gitlab.com/Budi721/mini-crm.git
```

### Installing Dependency
```ssh
composer install
npm install && npm run dev
```

### Setting Environment

Duplikasi file kan `.env.example`, setelah itu rename menjadi `.env`
adapun untuk settingan env sebagai berikut :

1. Pastikan di environment terdapat database dengan nama `mini_crm`
  - DB_CONNECTION=mysql
  - DB_HOST=127.0.0.1
  - DB_PORT=3306
  - DB_DATABASE=mini_crm
  - DB_USERNAME=root
  - DB_PASSWORD= 

2. Update setting mailtrap to resolve error when create company
  - MAIL_MAILER=
  - MAIL_HOST=
  - MAIL_PORT=
  - MAIL_USERNAME=
  - MAIL_PASSWORD=
  - MAIL_ENCRYPTION=
  - MAIL_FROM_NAME=

Jalankan perintah di bawah ini:
```php
php artisan migrate --seed
```


-- Selesai --
