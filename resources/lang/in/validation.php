<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Atribut :attribute harus dapat diterima.',
    'active_url' => 'Atribut :attribute bukan URL yang valid.',
    'after' => 'Atribut :attribute harus setelah :date.',
    'after_or_equal' => 'Atribut :attribute harus setelah atau sama dengan :date.',
    'alpha' => 'Atribut :attribute hanya boleh huruf.',
    'alpha_dash' => 'Atribut :attribute harus huruf, angka, dash and underscore.',
    'alpha_num' => 'Atribut :attribute harus huruf dan angka.',
    'array' => 'Atribut :attribute harus array.',
    'before' => 'Atribut :attribute harus sebelum :date.',
    'before_or_equal' => 'Atribut :attribute harus sebelum atau sama dengan :date.',
    'between' => [
        'numeric' => 'Atribut :attribute harus diantara :min dan :max.',
        'file' => 'Atribut :attribute harus diantara :min dan :max kilobytes.',
        'string' => 'Atribut :attribute harus diantara :min dan :max karakter.',
        'array' => 'Atribut :attribute harus diantara :min dan :max item.',
    ],
    'boolean' => 'Atribut :attribute field harus true or false.',
    'confirmed' => 'Atribut :attribute konfirmasi tidak sesuai.',
    'current_password' => 'Atribut password tidak sesuai.',
    'date' => 'Atribut :attribute tidak sesuai format tanggal.',
    'date_equals' => 'Atribut :attribute harus tanggal :date.',
    'date_format' => 'Atribut :attribute tidak sesuai format :format.',
    'different' => 'Atribut :attribute dan :other harus beda.',
    'digits' => 'Atribut :attribute harus :digits digits.',
    'digits_between' => 'Atribut :attribute harus diantara :min dan :max digits.',
    'dimensions' => 'Atribut :attribute dimensi tidak valid.',
    'distinct' => 'Atribut :attribute terdapat duplikasi nilai.',
    'email' => 'Atribut :attribute harus valid format email.',
    'ends_with' => 'Atribut :attribute harus diakhiri: :values.',
    'exists' => 'Atribut yang dipilih :attribute invalid.',
    'file' => 'Atribut :attribute harus berbentuk file.',
    'filled' => 'Atribut :attribute harus diisi.',
    'gt' => [
        'numeric' => 'Atribut :attribute harus lebih dari :value.',
        'file' => 'Atribut :attribute harus lebih dari :value kilobytes.',
        'string' => 'Atribut :attribute harus lebih dari :value karakter.',
        'array' => 'Atribut :attribute harus lebih dari :value item.',
    ],
    'gte' => [
        'numeric' => 'Atribut :attribute harus lebih dari atau sama dengan :value.',
        'file' => 'Atribut :attribute harus lebih dari atau sama dengan :value kilobytes.',
        'string' => 'Atribut :attribute harus lebih dari atau sama dengan :value karakter.',
        'array' => 'Atribut :attribute harus :value items atau lebih.',
    ],
    'image' => 'Atribut :attribute harus berupa gambar.',
    'in' => 'Atribut yang dipilih :attribute tidak valid.',
    'in_array' => 'Atribut :attribute tidak ada di :other.',
    'integer' => 'Atribut :attribute harus berupa integer.',
    'ip' => 'Atribut :attribute harus valid format IP address.',
    'ipv4' => 'Atribut :attribute harus valid format IPv4 address.',
    'ipv6' => 'Atribut :attribute harus valid format IPv6 address.',
    'json' => 'Atribut :attribute harus valid format JSON string.',
    'lt' => [
        'numeric' => 'Atribut :attribute harus kurang dari :value.',
        'file' => 'Atribut :attribute harus kurang dari :value kilobytes.',
        'string' => 'Atribut :attribute harus kurang dari :value karakter.',
        'array' => 'Atribut :attribute harus kurang dari :value items.',
    ],
    'lte' => [
        'numeric' => 'Atribut :attribute harus kurang dari atau sama dengan :value.',
        'file' => 'Atribut :attribute harus kurang dari atau sama dengan :value kilobytes.',
        'string' => 'Atribut :attribute harus kurang dari atau sama dengan :value characters.',
        'array' => 'Atribut :attribute tidak lebih dari :value items.',
    ],
    'max' => [
        'numeric' => 'Atribut :attribute harus lebih dari :max.',
        'file' => 'Atribut :attribute harus lebih dari :max kilobytes.',
        'string' => 'Atribut :attribute harus lebih dari :max karakter.',
        'array' => 'Atribut :attribute harus tidak lebih :max items.',
    ],
    'mimes' => 'Atribut :attribute harus file dengan tipe: :values.',
    'mimetypes' => 'Atribut :attribute harus file dengan tipe: :values.',
    'min' => [
        'numeric' => 'Atribut :attribute harus minimal :min.',
        'file' => 'Atribut :attribute harus minimal :min kilobytes.',
        'string' => 'Atribut :attribute harus minimal :min karakter.',
        'array' => 'Atribut :attribute harus paling tidak :min items.',
    ],
    'multiple_of' => 'Atribut :attribute harus kelipatan dari :value.',
    'not_in' => 'Atribut yang dipilih :attribute adalah tidak valid.',
    'not_regex' => 'Atribut :attribute format tidak valid.',
    'numeric' => 'Atribut :attribute harus berupa angka.',
    'password' => 'Atribut password tidak benar.',
    'present' => 'Atribut :attribute field harus ditampilkan.',
    'regex' => 'Atribut :attribute format tidak valid.',
    'required' => 'Atribut :attribute wajib diisi.',
    'required_if' => 'Atribut :attribute wajib diisi ketika :other adalah :value.',
    'required_unless' => 'Atribut :attribute wajib diisi kecuali :other adalah dalam :values.',
    'required_with' => 'Atribut :attribute wajib diisi ketika :values ditampilkan.',
    'required_with_all' => 'Atribut :attribute wajib diisi ketika :values ditampilkan.',
    'required_without' => 'Atribut :attribute wajib diisi ketika :values adalah tidak ditampilkan.',
    'required_without_all' => 'Atribut :attribute wajib diisi ketika tidak ada dari :values ditampilkan.',
    'prohibited' => 'Atribut :attribute field dilarang.',
    'prohibited_if' => 'Atribut :attribute field dilarang ketika :other adalah :value.',
    'prohibited_unless' => 'Atribut :attribute field dilarang kecuali :other adalah dalam :values.',
    'same' => 'Atribut :attribute dan :other tidak sama.',
    'size' => [
        'numeric' => 'Atribut :attribute harus :size.',
        'file' => 'Atribut :attribute harus :size kilobytes.',
        'string' => 'Atribut :attribute harus :size karakter.',
        'array' => 'Atribut :attribute harus mengandung :size items.',
    ],
    'starts_with' => 'Atribut :attribute harus dimulai dengan: :values.',
    'string' => 'Atribut :attribute harus berupa string.',
    'timezone' => 'Atribut :attribute harus valid format timezone.',
    'unique' => 'Atribut :attribute sudah dipakai.',
    'uploaded' => 'Atribut :attribute gagal untuk upload.',
    'url' => 'Atribut :attribute harus valid format URL.',
    'uuid' => 'Atribut :attribute harus valid format UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
