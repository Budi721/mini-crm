<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Kredensial ini tidak ada dalam database kami.',
    'password' => 'Password salah.',
    'throttle' => 'Terlalu banyak kesempatan. Coba lagi dalam :seconds detik.',

];
