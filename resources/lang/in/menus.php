<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    */
    'edit' => 'Edit',
    'delete' => 'Hapus',
    'back' => 'Kembali',
    'submit' => 'Kirim',
    'list' => 'Daftar',

    /**
     * app view component
     */
    'login' => 'Masuk',
    'logout' => 'Keluar',

    /**
     * login view
     */
    'email' => 'Alamat Email',
    'password' => 'Kata Sandi',
    'remember-me' => 'Ingat Saya',
    'forget' => 'Lupa Kata Sandi?',

    /**
     * email
     */
    'reset' => 'Atur Ulang Kata Sandi',
    'reset-link' => 'Kirim Link Atur Ulang Kata Sandi',

    /**
     * root view
     */
    'dashboard' => 'Dasbor',
    'status' => 'Kamu berhasil masuk!',
    'company-profile' => 'Profil Perusahaan',
    'employee-profile' => 'Profil Karyawan',

    /**
     * companies view
     */
    'create-company' => 'Buat Perusahaan Baru',
    'company' => 'Perusahaan',
    'company-name' => 'Nama Perusahaan',
    'company-logo' => 'Logo',
    'company-email' => 'Email',
    'company-web' => 'Laman Website',
    'company-show' => 'Lihat Detail',
    'company-createtitle' => 'Perusahaan Baru',
    'company-edittitle' => 'Ubah Perusahaan',
    'company-listemployee' => 'Daftar Karyawan',
    'company-noemployee' => 'Tidak ada karyawan di perusahaan ini',
    'company-pleaseupload' => 'Silahkan upload logo.',

    'employee' => 'Karyawan',
    'employee-createtitle' => 'Karyawan Baru',
    'employee-edittitle' => 'Ubah Data Karyawan',
    'employee-first' => 'Nama Depan',
    'employee-last' => 'Nama Belakang',
    'employee-email' => 'Email',
    'employee-phone' => 'No. Telp',

];
