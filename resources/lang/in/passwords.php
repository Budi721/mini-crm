<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Password telah direset!',
    'sent' => 'Kami telah mengirim link reset password melalui email!',
    'throttled' => 'Tunggu sebelum mencoba lagi.',
    'token' => 'Password reset token tidak valid.',
    'user' => "Kita tidak dapat mencari email tersebut di database.",

];
