<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'edit' => 'Edit',
    'delete' => 'Delete',
    'back' => 'Back',
    'submit' => 'Submit',
    'list' => 'List',
    /**
     * app view component
     */
    'login' => 'Login',
    'logout' => 'Logout',

    /**
     * login view
     */
    'email' => 'Email Address',
    'password' => 'Password',
    'remember-me' => 'Remember Me',
    'forget' => 'Forgot Your Password?',

    /**
     * email view
     */
    'reset' => 'Reset Password',
    'reset-link' => 'Send Password Reset Link',

    /**
     * root view
     */
    'dashboard' => 'Dashboard',
    'status' => 'You are logged in!',
    'company-profile' => 'Company Profile',
    'employee-profile' => 'Employee Profile',

    /**
     * companies view
     */
    'create-company' => 'New Company',
    'company' => 'Company',
    'company-name' => 'Company Name',
    'company-logo' => 'Badge',
    'company-email' => 'Email',
    'company-web' => 'Website',
    'company-show' => 'Show Detail',
    'company-createtitle' => 'New Company',
    'company-edittitle' => 'Edit Company',
    'company-listemployee' => 'List of Employee',
    'company-noemployee' => 'No employee in this company',
    'company-pleaseupload' => 'Please upload logo.',

    'employee' => 'Employee',
    'employee-createtitle' => 'New Employee',
    'employee-first' => 'Firstname',
    'employee-last' => 'Lastname',
    'employee-email' => 'Email',
    'employee-phone' => 'Phone Number',

];
