@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('menus.dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('menus.status') }}
                    <div class="d-grid gap-2 col-6 mx-auto">
                        <a href="/companies" class="btn btn-outline-dark">{{ __('menus.company-profile') }}</a>
                        <a href="/employees" class="btn btn-secondary">{{ __('menus.employee-profile') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
