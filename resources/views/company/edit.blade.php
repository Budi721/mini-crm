@extends("layouts.app")

@section('content')
  <center><h2>{{ __('menus.company-edittitle') }}</h2></center>
    <form action="/companies/{{ $company->id }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label for="name" class="form-label">{{ __('menus.company-name') }}</label>
      <input type="text" class="form-control" value="{{ old('name') ? old('name') : $company->name }}" id="name" name="name">
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="email" class="form-label">{{ __('menus.company-email') }}</label>
      <input type="email" class="form-control" value="{{ old('email') ? old('email') : $company->email }}" id="email" name="email">
      @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="website" class="form-label">{{ __('menus.company-web') }}</label>
      <input type="text" class="form-control" value="{{ old('website') ? old('website') : $company->website }}" id="website" name="website">
      @error('website')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="logo" class="form-label">{{ __('menus.company-logo') }}</label>
      <input type="file" class="form-control" value="{{ old('logo') }} id="logo" name="logo">
      @error('logo')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="box-image mt-2 ml-2">
        @if(!empty($company->logo))
        <img src="/images/{{ $company->logo }}" class="" width="100px">
        @else
        <small id="logoHelp" class="form-text text-muted">{{ __('menus.company-pleaseupload') }}</small>
        @endif
      </div>
    </div>

    <button onclick="window.history.go(-1);return false;" class="btn btn-danger">{{ __('menus.back') }}</button>
    <button type="submit" class="btn btn-primary">{{ __('menus.submit') }}</button>
  </form>
@endsection