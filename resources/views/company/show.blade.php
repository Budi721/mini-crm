@extends("layouts.app")

@section('content')
<div class="card mb-3">
  <img src="{{ asset('images/'.$company->logo) }}" class="card-img-top" 
    alt="{{ $company->name }}">
  <div class="card-body">
    <h5 class="card-title">{{ $company->name }}</h5>
    <p class="card-text"><strong class="col-lg-2">{{ __('menus.company-email') }} :</strong>{{ $company->email }}</p>
    <p class="card-text"><strong class="col-lg-2">{{ __('menus.company-web') }} :</strong>{{ $company->website }}</p>
    <p class="card-text col-lg-2">
      <strong>{{ __('menus.company-listemployee') }} :</strong>
        <ul>
        @forelse ($company->employee as $employee)
          <li>{{ $employee->first_name }} {{ $employee->last_name }}</li>  
          @empty
          <center> {{ __('menus.company-noemployee') }} </center>
        @endforelse
        </ul>
    </p>
  </div>
  </div>

  <a href="/companies/{{ $company->id }}/edit" class="btn btn-info btn-sm">{{ __('menus.edit') }}</a>
  <form action="/companies/{{ $company->id }}" method="post" class="d-inline">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger btn-sm">{{ __('menus.delete') }}</button>
  </form>
<a href="/companies" class="btn btn-danger btn-sm float-right">{{ __('menus.back') }}</a>
@endsection