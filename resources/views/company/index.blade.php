@extends("layouts.app")

@section('content')
  <center>
    <h2 class="mb-3">{{ __('menus.company-profile') }}</h2>
    <a class="btn btn-secondary" href="/companies/create">
      {{ __('menus.create-company') }}
    </a>
  </center>
  
  @foreach ($companies->chunk(2) as $companiesChunk)
  <div class="row">
    @foreach ($companiesChunk as $company)
    <div class="box bg-white br-sm mb-4 mb-lg-0">
    </div>
    <div class="card-body col-lg-6">
      <div class="table-responsive">
        <table class="table table-bordered">
          <tr>
            <th class="col-lg-3">{{ __('menus.company-name') }}</th>
            <td>{{ $company->name }}</td>
          </tr>
          <tr>
            <th class="col-lg-3">{{ __('menus.company-email') }}</th>
            <td>{{ $company->email }}</td>
          </tr>
          <tr>
            <th class="col-lg-3">{{ __('menus.company-logo') }}</th>
            <td>
              <img src="{{ asset('images/'.$company->logo) }}" 
                alt="{{ $company->name }}" class="img" width="100px">
            </td>
          </tr>
          <tr>
            <th class="col-lg-3">{{ __('menus.company-web') }}</th>
            <td>{{ $company->website }}</td>
          </tr>
        </table>
        <a href="/companies/{{ $company->id }}" class="btn btn-info">{{ __('menus.company-show') }}</a>
      </div>
    </div>
    @endforeach
  </div>

  @endforeach

  <div class="d-flex justify-content-center">{{ $companies->links() }}</div>
@endsection