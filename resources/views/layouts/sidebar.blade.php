<div class="menu">
  <div class="main-menu">
      <div class="scroll">
          <ul class="list-unstyled">
              <li class="active">
                  <a href="#menu">
                      <i class="iconsminds-three-arrow-fork"></i> Menu
                  </a>
              </li>
          </ul>
      </div>
  </div>
  <div class="sub-menu">
      <div class="scroll">
          <ul class="list-unstyled" data-link="menu" id="menuTypes">
              <li>
                  <a href="#" data-toggle="collapse" data-target="#collapseMenuTypes" aria-expanded="true"
                      aria-controls="collapseMenuTypes" class="rotate-arrow-icon">
                      <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">{{ __('menus.company') }}</span>
                  </a>
                  <div id="collapseMenuTypes" class="collapse show" data-parent="#menuTypes">
                      <ul class="list-unstyled inner-level-menu">
                          <li>
                              <a href="/companies">
                                  <i class="simple-icon-drawer"></i><span
                                      class="d-inline-block">{{ __('menus.list') }}</span>
                              </a>
                          </li>
                          <li>
                              <a href="/companies/create">
                                  <span
                                      class="d-inline-block">{{ __('menus.company-createtitle') }}</span>
                              </a>
                          </li>
                      </ul>
                  </div>
              </li>

              <li>
                  <a href="#" data-toggle="collapse" data-target="#collapseMenuDetached" aria-expanded="true"
                      aria-controls="collapseMenuDetached" class="rotate-arrow-icon collapsed">
                      <i class="simple-icon-arrow-down"></i> <span class="d-inline-block">{{ __('menus.employee') }}</span>
                  </a>
                  <div id="collapseMenuDetached" class="collapse">
                      <ul class="list-unstyled inner-level-menu">
                          <li>
                              <a href="/employees">
                                  <i class="simple-icon-people"></i>
                                  <span class="d-inline-block">{{  __('menus.list') }}</span>
                              </a>
                          </li>
                          <li>
                              <a href="/employees/create">
                                  <span class="d-inline-block">{{ __('menus.employee-createtitle') }}</span>
                              </a>
                          </li>
                      </ul>
                  </div>
              </li>
          </ul>
      </div>
  </div>
</div>