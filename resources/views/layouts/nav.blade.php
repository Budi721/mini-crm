<nav class="navbar fixed-top">
  
  <div class="d-flex align-items-center navbar-left">
    @auth
      <a href="#" class="menu-button d-none d-md-block">
          <svg class="sub" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
              <rect x="0" y="0.5" width="26" height="1" />
              <rect x="0" y="7.5" width="26" height="1" />
              <rect x="0" y="15.5" width="26" height="1" />
          </svg>
      </a>
    @endauth
  </div>  

  <a class="navbar-brand" href="/">
    {{ config('app.name', 'Mini CRM') }}
  </a>

  <div class="navbar-right">
      <div class="header-icons d-inline-block align-middle">
          <li class="nav-item dropdown d-md-inline-block align-text-bottom mr-3">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ Config::get('languages')[App::getLocale()] }}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            @foreach (Config::get('languages') as $lang => $language)
                @if ($lang != App::getLocale())
                        <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}"> {{$language}}</a>
                @endif
            @endforeach
            </div>
          </li>
          <button class="header-icon btn btn-empty d-none d-sm-inline-block" type="button" id="fullScreenButton">
              <i class="simple-icon-size-fullscreen"></i>
              <i class="simple-icon-size-actual"></i>
          </button>
      </div>


      
      <div class="user d-inline-block">
        @guest
          @if (Route::has('login'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('menus.login') }}</a>
                </li>
            @endif
        @else
          <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true"
              aria-expanded="false">
              <span class="name">{{ Auth::user()->name }}</span>
              <span>
                  <img alt="Profile Picture" src="{{ asset('img/profiles/l-1.jpg') }}" />
              </span>
          </button>

          <div class="dropdown-menu dropdown-menu-right mt-3">
            <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                {{ __('menus.logout') }}
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
          </div>
        @endguest
      </div>
  </div>
</nav>