@extends('layouts.app')

@section('content')
  <center class="mb-3">
    <h2>{{ __('menus.employee-createtitle') }}</h2>
  </center>
  <form action="/employees" method="POST" enctype="multipart/form-data">
  @csrf
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="fist_name">{{ __('menus.employee-first') }}</label>
      <input type="text" class="form-control" name="first_name" placeholder="{{ __('menus.employee-first') }}" value="{{ old('first_name') }}">
      @error('first_name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="form-group col-md-6">
      <label for="fist_name">{{ __('menus.employee-last') }}</label>
      <input type="text" class="form-control" name="last_name" placeholder="{{ __('menus.employee-last') }}" value="{{ old('last_name') }}">
      @error('last_name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div class="form-group">
    <label for="email">{{ __('menus.employee-email') }}</label>
    <input type="text" class="form-control" name="email" placeholder="example@mail.net" value="{{ old('email') }}">
  </div>
  <div class="form-group">
    <label for="phone">{{ __('menus.employee-phone') }}</label>
    <input type="text" class="form-control" name="phone" placeholder="0812xxxxxxx" value="{{ old('phone') }}">
  </div>
  <div class="form-group">
    <label for="company">{{ __('menus.company-name') }}</label>
    <select id="company" name="company_id" class="form-control">
        <option selected>Choose...</option>
        @foreach ($companies as $company)
        @if (old('company_id') == $company->id)
          <option value="{{ $company->id }}" selected>{{ $company->name }}</option>
        @else
          <option value="{{ $company->id }}">{{ $company->name }}</option>
        @endif
        @endforeach
    </select>
  </div>
  <button onclick="window.history.go(-1);return false;" class="btn btn-danger">{{ __('menus.back') }}</button>
  <button type="submit" class="btn btn-primary">{{ __('menus.submit') }}</button>
  </form>
  
@endsection