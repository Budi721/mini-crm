@component('mail::message')
# New Company Registered

Your order has been shipped!

@component('mail::button', ['url' =>  route('companies.index')])
View Dashboard
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

{{-- TEST EMAIL --}}